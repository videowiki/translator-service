const { server, app } = require('./generateServer')();
const transtionVendor = require('./vendors/translation')

app.get('/health', (req, res) => {
    transtionVendor.google.translateText('hi', 'hi')
    .then(() => {
        return res.status(200).send('OK');
    })
    .catch(err => {
        console.log(err);
        return res.status(503).send('GOOGLE TRANSLATION DOWN');
    })
})

app.post('/', (req, res) => {
    const { text, to } = req.body;
    transtionVendor.google.translateText(text, to)
        .then((translatedText) => {
            return res.json({ text, translatedText });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send('Something went wrong');
        })
})


const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
